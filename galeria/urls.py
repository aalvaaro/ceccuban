from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^$', 'website.views.galeria'),
    url(r'^adopciones/$', 'website.views.adopciones'),
    url(r'^esterilizaciones/$', 'website.views.esterilizaciones'),
    url(r'^pintas/$', 'website.views.pintas'),

)

