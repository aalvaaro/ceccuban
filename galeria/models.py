# -*- coding: utf-8 -*-

from django.db import models
from cloudinary.models import CloudinaryField
from autoslug import AutoSlugField
from randomslugfield import RandomSlugField

class Adopcion(models.Model):
    nombre = models.CharField(max_length = 100, unique = True)
    # Generate the choices key pairs and then the atribute, this will apply for
    # sexo and tamaño
    SEXO_CHOICES = (("macho", "macho"), ("hembra", "hembra"))
    SIZE_CHOICES = (("chico", "chico"), ("mediano", "mediano"), ("grande", "grande"))
    TIPO_CHOICES = (("perro", "perro"), ("gato", "gato"))
    sexo = models.CharField(max_length = 20, choices = SEXO_CHOICES)
    tipo = models.CharField(max_length = 10, choices = TIPO_CHOICES)
    tamano = models.CharField(max_length = 20, choices = SIZE_CHOICES)
    foto = CloudinaryField("foto")
    disponible = models.BooleanField(default = True)
    descripcion = models.TextField(help_text = "Escribir información sobre el perrito")
    slug = AutoSlugField(populate_from = "nombre", always_update = True)

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Adopciones"

class Pinta(models.Model):
    imagen = CloudinaryField()
    slug = RandomSlugField(length = 7)

class Esterilizacion(models.Model):
    imagen = CloudinaryField()
    slug = RandomSlugField(length = 7)

    class Meta:
        verbose_name_plural = "Esterilizaciones"




