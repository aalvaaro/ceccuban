from django.contrib import admin
from .models import Adopcion, Pinta, Esterilizacion

class AdopcionAdmin(admin.ModelAdmin):
    list_filer = ("nombre", "sexo", "tamano",)

    class Media:
        js = ("js/tiny_mce/tiny_mce.js",
              "js/basic_config.js",)


admin.site.register(Adopcion, AdopcionAdmin)
admin.site.register(Pinta)
admin.site.register(Esterilizacion)



