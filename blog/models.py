from django.db import models
from autoslug import AutoSlugField
from cloudinary.models import CloudinaryField

class Noticia(models.Model):
    titulo = models.CharField(max_length = 100)
    fecha = models.DateField(auto_now_add = True)
    imagen = CloudinaryField("imagen")
    contenido = models.TextField()
    slug = AutoSlugField(populate_from = "titulo", always_update = True,
                         unique_with = "id")

    def __unicode__(self):
        return self.titulo
