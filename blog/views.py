from django.shortcuts import render_to_response
from .models import Noticia
from galeria.models import Adopcion

def noticias(request):
    noticias = Noticia.objects.all()
    disponibles = Adopcion.objects.all()
    context = {"noticias": noticias, "disponibles": disponibles}
    return render_to_response("blog.html", context)

def noticia(request, slug):
    noticia = Noticia.objects.get(slug = slug)
    disponibles = Adopcion.objects.all()
    noticias = Noticia.objects.all()
    context = {"noticia": noticia, "noticias": noticias, "disonibles": disponibles}
    return render_to_response("post.html", context)
