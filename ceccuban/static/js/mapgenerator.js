jQuery(document).ready(function($) {
	$("#responsive_map").gMap({
		 maptype: google.maps.MapTypeId.ROADMAP, 
		 zoom: 18, 
		 markers: [{
			 latitude: 19.795607, 
			 longitude: -101.17643, 
			 html: "<h5>Ceccuban Tarímbaro</h5>", 
			 popup: true, 
			 flat: true, 
			 icon: { 
				 image: "icons/pets.png", 
				 iconsize: [32, 37], 
				 iconanchor: [15, 30], 
				 shadow: "icons/icon-shadow.png", 
				 shadowsize: [32, 37], 
				 shadowanchor: null}
				} 
			], 
		 panControl: false, 
		 zoomControl: true, 
		 mapTypeControl: false, 
		 scaleControl: false, 
		 streetViewControl: true, 
		 scrollwheel: false, 
		 styles: [ { "stylers": [ { "hue": "#b4ac2b" }, { "gamma": 1.58 } ] } ], 
		 onComplete: function() {			
			 var gmap = $("#responsive_map").data('gmap').gmap;
			 window.onresize = function(){
				 google.maps.event.trigger(gmap, 'resize');
				 $("#responsive_map").gMap('fixAfterResize');
			 };
		}
	});
});