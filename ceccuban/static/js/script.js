$(document).ready(function() {
	
	// Contact Maps
	$("#maps").gmap3({
		map: {
			options: {
			  center: [19.795607,-101.17643],
			  zoom: 18,
			  scrollwheel: true
			}  
		 },
		marker:{
			latLng: [-7.866315,110.389574],
			options: {
			 icon: new google.maps.MarkerImage(
			   "https://dl.dropboxusercontent.com/u/29545616/Preview/marker.png",
			   new google.maps.Size(48, 48, "px", "px")
			 )
			}
		 }		 
	});
	
	//Slider
	$("#slider").carousel({
		interval: 2000
	});
	
	$("#testi").carousel({
		interval: 2000
	});
	
	$("#itemsingle").carousel({
		interval: true
	});
});
