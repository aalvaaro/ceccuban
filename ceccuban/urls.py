from django.conf.urls import patterns, include, url
import grappelli

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),

    # URLs del sitio
    url(r'^$', 'website.views.index'),
    url(r'^quienes_somos/$', 'website.views.about'),
    url(r'^galeria/$', 'website.views.galeria'),
    url(r'^contacto/$', 'website.views.contacto'),
    url(r'^noticias/$', 'blog.views.noticias'),
    url(r'^noticias/(?P<slug>\D+)/$', 'blog.views.noticia'),

    url(r'^galeria/adopciones$', 'website.views.adopciones'),
    url(r'^galeria/adopciones/(?P<slug>\D+)/$', 'website.views.adopciones_detalle'),
    url(r'^galeria/esterilizaciones/$', 'website.views.esterilizaciones'),
    url(r'^galeria/pintas/$', 'website.views.pintas'),
)
