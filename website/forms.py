from django import forms

class ContactForm(forms.Form):
    nombre = forms.CharField()
    nombre.widget = forms.TextInput(attrs = {"class": "span12",
                                             "name": "nombre",
                                             "required": "true",
                                             "placeholder": "Escribe tu nombre",
                                             "id": "nombre"})
    
    email = forms.CharField()
    email.widget = forms.EmailInput(attrs = {"class": "span12",
                                             "name": "email",
                                             "required": "true",
                                             "placeholder": "Escribe tu email",
                                             "id": "email"})
    mensaje = forms.CharField()
    mensaje.widget = forms.Textarea(attrs = {"name": "mensaje",
                                             "class": "span12",
                                             "size": 100,
                                             "rows": 10,
                                             "required": "true",
                                             "placeholder": "Escribe el mensaje"})