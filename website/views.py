# -*- coding: utf-8 -*-

from django.shortcuts import render, render_to_response
from galeria.models import Adopcion, Esterilizacion, Pinta
from blog.models import Noticia
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from .forms import ContactForm
from django.core.mail import send_mail
from django.http import HttpResponseRedirect

def index(request):
    disponibles = Adopcion.objects.order_by("-id")
    noticias = Noticia.objects.order_by("-fecha")
    context = {"disponibles": disponibles, "noticias": noticias}
    return render_to_response("index.html", context)

def about(request):
    return render_to_response("about.html")

def galeria(request):
    return render_to_response("galeria.html")

def adopciones(request):
    disponibles = Adopcion.objects.all()
    context = {"disponibles": disponibles}
    return render_to_response("adopciones.html", context)

def adopciones_detalle(request, slug):
    adopcion = Adopcion.objects.get(slug = slug)
    disponibles = Adopcion.objects.all()
    context = {"adopcion": adopcion, "disponibles": disponibles}
    return render_to_response("detalles.html", context)

def esterilizaciones(request):
    lista_esterilizaciones = Esterilizacion.objects.all()
    paginator = Paginator(lista_esterilizaciones, 12)

    page = request.GET.get("page")
    # Make a request for getting the actual page
    try:
        esterilizaciones = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page
        esterilizaciones = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page or results
        esterilizaciones = paginator.page(paginator.num_pages)
    context = {"esterilizaciones": esterilizaciones}
    return render_to_response("esterilizaciones.html", context)

def pintas(request):
    lista_pintas = Pinta.objects.all()
    paginator = Paginator(lista_pintas, 12)

    page = request.GET.get("page")

    # Make a request for getting the actual page number
    try:
        pintas = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page
        pintas = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 99999), deliver last page of results
        pintas = paginator.page(paginator.num_pages)
    context = {"pintas": pintas}
    return render_to_response("pintas.html", context)

    context = {"posts": posts, "disponibles": disponibles, "noticias": noticias}
    return render_to_response("posts.html", context)

    return render_to_response("post.html", context)

def contacto(request):
    if request.method == "POST":
        form = ContactForm(request.POST)
        if form.is_valid():
            nombre = form.cleaned_data["nombre"]
            email = form.cleaned_data["email"]
            mensaje = form.cleaned_data["mensaje"]

            # Enviar el mail para recibir autorespuesta
            message = "%s ha contactado a traves del sitio del ceccuban.\n--------------------------\n \
            El mensaje es el siguiente:\n -------------------------------------\n%s \n\n \
            Responder al siguiente correo: %s" % (nombre, mensaje, email)
            send_mail("Contacto ceccuban.com", message, "contacto@ceccuban.com", ["aadelgado@fevaq.net"], fail_silently = False)
            return HttpResponseRedirect("/contacto/" )
    else:
        form = ContactForm()
    context = {"form": form}
    return render(request, "contacto.html", context)












